import React from 'react'
import classes from './Control.css'


const control = (props) =>{








	return (

		<div className={classes.Control}>
			<span className={classes.ingredientName}>{props.ingredient}</span>
			<span className={classes.Button} onClick={()=>props.changeBurger(props.ingredient , "+")}>+</span>
			<span className={classes.Button} onClick={()=>props.changeBurger(props.ingredient , "-")}>-</span>
		</div>

	)

}


export default control;