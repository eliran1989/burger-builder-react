import React from 'react'
import Control from './Control/Control'
import classes from './BuildControls.css'

const buildControls = (props) =>{

		let ingredientsArray = Object.keys(props.ingredients);
		let controls = [];


				ingredientsArray.forEach((item , index)=>{
						controls.push(<Control key={item+index} ingredient={item} changeBurger={props.changeBurger}/>);
				});




	
		return (

			<div className={classes.BuildControls}>
			{controls}
			</div>
		)

}



export default buildControls;