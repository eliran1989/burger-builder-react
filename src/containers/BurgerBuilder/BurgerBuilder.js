import React , {Component} from 'react';
import Auxiliary from '../../hoc/Auxiliary';
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/BuildControls/BuildControls';

class BurgerBuilder extends Component {

	state = {
		ingredients:{
			salad:0,
			bacon:0,
			cheese:0,
			meat:0,
		}

	}




		changeBurgerListener = (ingredient , operator) => {

				let ingredients = this.state.ingredients;

				if(operator==="+"){
					ingredients[ingredient]++;
				}else{
					if(ingredients[ingredient]!==0)
					ingredients[ingredient]--;
				}

				this.setState(ingredients);

		}




	render(){

		return (
			<Auxiliary>
			<Burger ingredients={this.state.ingredients}/>
			<BuildControls changeBurger={this.changeBurgerListener} ingredients={this.state.ingredients}/>
			</Auxiliary>
		)
	}


}


export default BurgerBuilder;